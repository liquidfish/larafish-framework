<?php

namespace spec\Liquidfish\Larafish\Operation;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class OperationSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Liquidfish\Larafish\Operation\Operation');
    }

	function it_is_unsuccessful_when_first_initialized()
	{
		$this->wasSuccessful()->shouldBe(false);
	}

	function it_is_able_to_be_successful()
	{
		$this->succeeded();
		$this->wasSuccessful()->shouldBe(true);
	}

	function it_is_able_to_be_unsuccessful()
	{
		$this->wasSuccessful()->shouldBe(false);
	}

	function it_can_have_a_message()
	{
		$this->setMessage('Message!');
		$this->getMessage()->shouldReturn('Message!');
	}

	function it_accepts_custom_data()
	{
		$this->set('key', 'value');
		$this->getData()->shouldHaveKey('key');
		$this->getData()->shouldHaveValue('value');
	}

	function it_should_not_accept_custom_data_with_an_empty_key()
	{
		$this->set('', 'value');
		$this->getData()->shouldNotHaveKey('');
	}

	function it_can_have_message_details_added()
	{
		$this->setMessageDetails(array('detail one', 'detail two'));
		$this->getMessageDetails()->shouldHaveValue('detail one');
		$this->getMessageDetails()->shouldHaveValue('detail two');
	}

	function it_returns_valid_json()
	{
		$sample = json_encode(array('success' => true, 'message' => 'Message', 'messageDetails' => array('detail one', 'detail two'), 'key' => 'value'));
		$this->succeeded();
		$this->setMessage('Message');
		$this->setMessageDetails(array('detail one', 'detail two'));
		$this->set('key', 'value');
		$this->asJson()->shouldEqual($sample);
	}

	public function it_returns_the_correct_value_for_additional_data_key()
	{
		$this->set('key', 'value');
		$this->get('key')->shouldReturn('value');
	}

	public function it_returns_null_for_non_existant_additional_key()
	{
		$this->get('key')->shouldReturn(null);
	}

	public function getMatchers()
	{
		return [
			'haveKey' => function($subject, $key) {
				return array_key_exists($key, $subject);
			},
			'haveValue' => function($subject, $value) {
				return in_array($value, $subject);
			},
			'notHaveKey' => function($subject, $key) {
				return !array_key_exists($key, $subject);
			},
			'notHaveValue' => function($subject, $value) {
				return !in_array($value, $subject);
			},
		];
	}
}
