<?php

use Illuminate\Database\Migrations\Migration;

class CreatePageComponentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('page_components',function($t)
		{
			$t->engine = 'InnoDB';
			$t->increments('id');
			$t->integer('page_id',false,true);
			$t->foreign('page_id')->references('id')->on('pages')->onDelete('cascade')->onUpdate('cascade');
			$t->integer('component_id',false,true);
			$t->foreign('component_id')->references('id')->on('components')->onDelete('cascade')->onUpdate('cascade');
			$t->integer('page_component_data_id',false,true);
			$t->foreign('page_component_data_id')->references('id')->on('page_component_data')->onDelete('cascade')->onUpdate('cascade');
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('page_components');
	}

}