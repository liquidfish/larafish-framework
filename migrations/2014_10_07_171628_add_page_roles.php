<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddPageRoles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pages', function($table)
		{
			$table->text('required_role_redirect_to')->nullable();
			$table->boolean('hidden')->default(0);;
		});

		Schema::create('page_roles', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('page_id', false, true);
			$table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
			$table->integer('role_id', false ,true);
			$table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pages', function($table)
		{
			$table->dropColumn('required_role_redirect_to');
			$table->dropColumn('hidden');
		});
		Schema::drop('page_roles');
	}

}