<?php namespace Liquidfish\Larafish\Error;

use View;
use Response;

class NotFound {

	/**
	 * Static shortcut
	 * @return mixed
	 */
	public static function showError()
	{
		$controller = new NotFound();
		return $controller->index();
	}

	/**
	 * Not Found Page
	 * @return mixed
	 */
	public function index()
	{
		$layout = View::make('layout');
		$layout->title = 'Not Found';
		$layout->yield = View::make('larafish::not-found');
		return Response::make($layout->render(), 404, array());
	}

}