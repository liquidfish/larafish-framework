<?php namespace Liquidfish\Larafish\Page;

use Larafish, Assets, View, Redirect;
use Liquidfish\Larafish\Page\Navigation\Breadcrumb;
use Liquidfish\Larafish\Routing\Uri;
use Liquidfish\Larafish\Page\Repository as PageRepository;

class Controller extends BaseController {

	public $layout = 'layout';

	public function __construct(Uri $uri, PageRepository $page, Breadcrumb $breadcrumb)
	{
		$this->uri = $uri;
		$this->page = $page;
		$this->breadcrumb = $breadcrumb;
	}

	/**
	 * Generate the View and Assets for this Page
	 * @return View A view object
	 */
	public function generate()
	{
		$uri = $this->uri->determine();

		# Load our page
		$this->page->forUri($uri);

		# Title
		$this->layout->title = $this->page->data->title;

		# Body Classes
		$this->layout->body_classes = $this->bodyClasses();

		# Meta Tags
        $this->layout->meta_tags = $this->page->data->meta_tags;

		$this->content = $this->contentView();

		if($this->page->data->type !== 'home')
		{
			$this->layout->breadcrumbs = View::make('larafish::breadcrumbs')->with('crumbs', $this->breadcrumb->forPage($this->page->data));
		}

		# Give the Page Content View the Page Data
		$this->content->page = $this->page->data;

		$this->checkForRedirect();

		# Give the content view its components
		$this->content->components = $this->page->components;

		# Add page settings for admins

		if(Larafish::userHasRole('Admin') or Larafish::userHasRole('Architect'))
		{
			$this->layout->page_settings = View::make('larafish::admin.pages.settings')->with('page', $this->page->data);
			Assets::appendString('<script>window.page_id = '.$this->page->data->id.';</script>');
		}
		if(Larafish::userHasRole('Architect'))
		{
			$this->layout->page_settings->component_settings = View::make('larafish::admin.pages.component-settings')->with('components', $this->page->components->current())->with('available',$this->page->components->available())->with('page_id', $this->page->data->id);
		}

		return $this->content;
	}

	protected function checkForRedirect(){
        if($this->page->data->roles->count())
        {
            if(!Larafish::userHasRole($this->page->data->roles->lists('name')))
            {
                header('Location: ' . $this->page->data->missing_role_redirect_url, true, 302);
                die();
            }
        }
    }

	/**
	 * Generate the body classes for this view
	 * @return string
	 */
	public function bodyClasses()
	{
		$classes = array();
		if($this->uri->getPrimary() == null)
		{
			$classes[] = 'home';
		}
		else
		{
			$classes[] = trim($this->uri->getPrimary().' '.$this->uri->getSecondary().' '.$this->uri->getTertiary());
		}

		$classes[] = trim($this->page->data->extra_body_classes);

		return implode(' ', $classes);
	}

	/**
	 * Create the content view
	 * @return View Content view for this page
	 */
	public function contentView()
	{
		if(str_contains($this->page->data->view, '::'))
		{
			return View::make($this->page->data->view);
		}
		else
		{
			return View::make('pages.'.$this->page->data->view);
		}
	}

	/**
	 * View the page
	 * @return void
	 */
	public function index()
	{
		$this->layout->yield = $this->generate();
	}
}
