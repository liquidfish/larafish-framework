<?php namespace Liquidfish\Larafish\Page;

use Config, Cache, DB, Exception, Larafish, View, URL, App;
use Liquidfish\Larafish\Page\Component\Component as PageComponent;
use Liquidfish\Larafish\Page\Component\Data\Data as PageComponentData;

class Page extends \Eloquent {

	protected $guarded = array('id');

	protected $appends = array('uri','type', 'missing_role_redirect_url');

	/**
	 * Components relationship
	 * @return object Related components
	 */
	public function components()
	{
		return $this->hasMany('Liquidfish\Larafish\Page\Component\Component','page_id')
            ->orderBy('sort_order','desc');
	}

	/**
	 * @return mixed
	 */
	public function roles()
	{
		return $this->belongsToMany('Liquidfish\Larafish\Models\Role', 'page_roles');
	}

	/**
	 * Get the full URI for the current page
	 * @return string Absolutely formatted URI - /one/two/three
	 */
	public function getUriAttribute()
	{
		$data = array();

		if(!empty($this->primary_uri)) $data[] = $this->primary_uri;
		if(!empty($this->secondary_uri)) $data[] = $this->secondary_uri;
		if(!empty($this->tertiary_uri)) $data[] = $this->tertiary_uri;

		return URL::route('page', $data, false);
	}

	/**
	 * See if the current page is the home page
	 * @return boolean
	 */
	public function isHome()
	{
		return ($this->primary_uri === null and $this->secondary_uri === null and $this->tertiary_uri === null) ? true : false;
	}

	/**
	 * @return mixed
	 */
	public function getMissingRoleRedirectUrlAttribute()
	{
		return (!empty($this->required_role_redirect_to) ? URL::to($this->required_role_redirect_to) : URL::route('home') );
	}

	/**
	 * Get the page type
	 * @return string
	 */
	public function getTypeAttribute()
	{
		if($this->primary_uri == null and $this->secondary_uri == null and $this->tertiary_uri == null) return 'home';
		if($this->primary_uri != null and $this->secondary_uri == null and $this->tertiary_uri == null) return 'primary';
		if($this->primary_uri != null and $this->secondary_uri != null and $this->tertiary_uri == null) return 'secondary';
		if($this->primary_uri != null and $this->secondary_uri != null and $this->tertiary_uri != null) return 'tertiary';
	}

	/**
	 * @param Page $page
	 */
	public function parents()
	{
		$query = DB::table('pages');

		switch($this->type)
		{
			case 'home':
			case 'primary':
				return array();
				break;
			case 'secondary':
				$query->where('primary_uri', $this->primary_uri)->whereNull('secondary_uri');
				break;
			case 'tertiary':
				$query->whereRaw(
					"(primary_uri = ? and secondary_uri = ? and tertiary_uri is null) or (secondary_uri is null and primary_uri = ?)",
					array($this->primary_uri, $this->secondary_uri, $this->primary_uri)
				);
				break;
		}

		return $query->get();

	}

	/**
	 * Build a Page object with all its related page components and their data
	 * @param  string $primary_uri   Primary URI
	 * @param  string $secondary_uri Secondary URI
	 * @param  string $tertiary_uri  Tertiary URI
	 * @param  bool $published_only
	 * @return Page
	 */
	public static function withComponents($primary_uri, $secondary_uri=null, $tertiary_uri=null, $published_only)
	{

		$model = new Page;

		$uri = $primary_uri;
		if($uri != null) $uri = '/'.$uri;
		if($secondary_uri != null) $uri .= '/'.$secondary_uri;
		if($tertiary_uri != null) $uri .= '/'.$tertiary_uri;

		$uri = 'page:'.$uri.($published_only ? ':published' : '' );

		if(Larafish::userCan('manage_pages'))
		{
			$page = $model->forUri($primary_uri, $secondary_uri, $tertiary_uri, $published_only);
		}
		else
		{
			$page = Cache::remember($uri, 15, function() use ($model, $primary_uri, $secondary_uri, $tertiary_uri, $published_only)
			{
				return $model->forUri($primary_uri ,$secondary_uri, $tertiary_uri, $published_only);
			});
		}

		return $page;

	}

	public function forUri($primary_uri, $secondary_uri=null, $tertiary_uri=null,$published_only)
	{
		$page = Page::where('primary_uri', $primary_uri)
			->where('secondary_uri', $secondary_uri)
			->where('tertiary_uri', $tertiary_uri);

		if($published_only) $page->where('published', 1);

        try {
    		$page = $page->firstOrFail();
	    	$page->load('components','components.component','components.data', 'roles');
        } catch (Exception $e) {
            App::abort(404);
        }

		return $page;
	}

}
