<?php namespace Liquidfish\Larafish\Page;

use Cache, Exception, Larafish;
use \Liquidfish\Larafish\Page\Component\Repository as PageComponentRepository;
use Liquidfish\Larafish\Page\Component\ComponentBag;

class Repository {

	public $data;
	public $content;
	public $components;

	protected $componentDataRepository;

	public function __construct(ComponentBag $components)
	{
		$this->components = $components;
	}

	/**
	 * Load a page with its components
	 * @param  string $uri URI for the page
	 * @return Repository
	 */
	public function forUri($uri)
	{
		$published_only = true;

		if (Larafish::userCan('manage_pages'))
		{
			$published_only = false;
		}

		$this->data = Page::withComponents($uri['primary_uri'],$uri['secondary_uri'],$uri['tertiary_uri'], $published_only);

		# Build components
		foreach($this->data->components as $component)
		{
			$this->components->add($component->component->view, (json_decode($component->data->data,true)?:[]) + ['component' => $component]);
		}

		return $this;
	}

	/**
	 * Delete a Page
	 * @param int $id
	 * @return \Liquidfish\Larafish\Page\Page
	 */
	public function delete($id)
	{
		$page = Page::find($id);
		$page->delete();

		$this->clearCacheById($page->id);

		return $page;
	}

	/**
	 * Get all pages seperated by type
	 * @return stdClass
	 */
	public function allPagesByType()
	{
		$pages = new \stdClass;

		$pages->primaryPages = Page::whereNotNull('primary_uri')->whereNull('secondary_uri')->whereNull('tertiary_uri')->orderBy('priority','asc')->with('roles')->get();
		$pages->secondaryPages = Page::whereNotNull('secondary_uri')->whereNull('tertiary_uri')->orderBy('priority','asc')->with('roles')->get();
		$pages->tertiaryPages = Page::whereNotNull('tertiary_uri')->orderBy('priority','asc')->with('roles')->get();

		return $pages;
	}

	/**
	 * Search page content
	 * @param $query
	 * @return mixed
	 */
	public function search($query)
	{
		return Page::where('content', 'like', '%' . $query . '%')->get();
	}

	/**
	 * Clear the cache for a Page by ID
	 * @param $id
	 * @return bool
	 */
	public function clearCacheById($id)
	{
		try
		{
			$page = Page::find($id);

			$page_uri = $page->uri;

			if($page_uri === '/')
			{
				$page_uri = '';
			}

			$uri = 'page:'.$page_uri;

			Cache::forget('pages');
			Cache::forget($uri);
			Cache::forget($uri.':published');

			return true;
		}
		catch(Exception $e)
		{
			return false;
		}
	}

}
