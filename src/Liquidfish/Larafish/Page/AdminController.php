<?php namespace Liquidfish\Larafish\Page;

use App, Cache, DB, Input, Redirect, Request, Response, URL, View;
use Liquidfish\Larafish\Page\Navigation\Navigator;
use Liquidfish\Larafish\Page\Repository as PageRepository;
use stdClass;
use Exception;
use Illuminate\Support\Str;
use Liquidfish\Larafish\Page\Page;

class AdminController extends BaseController {

	protected $pageRepository;
	protected $pageNavigator;

	function __construct(PageRepository $pageRepository, Navigator $pageNavigator)
	{
		$this->pageRepository = $pageRepository;
		$this->pageNavigator = $pageNavigator;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$this->layout->title = 'Page Manager';
		$this->layout->yield = View::make('larafish::admin.pages.list')->with('pages', $this->pageNavigator->map());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$response = array('success' => 0);

		if(Request::ajax())
		{

			try {

				$input = Input::all();

				$uri = str_slug( strip_tags( str_replace('&', 'and', $input['title']) ) );

				if(isset($input['parent_page_id']))
				{
					$parent_page = Page::find($input['parent_page_id']);
				}

				$new_page_data = array(
					'title' => $input['title']
				);

				switch ($input['type']) {
					case 'primary':
						$new_page_data['primary_uri'] = $uri;
						$response['uri'] = '/'.$uri;
						break;
					case 'secondary':
						$new_page_data['primary_uri'] = $parent_page->primary_uri;
						$new_page_data['secondary_uri'] = $uri;
						$response['uri'] = '/'.$parent_page->primary_uri.'/'.$uri;
						break;
					case 'tertiary':
						$new_page_data['primary_uri'] = $parent_page->primary_uri;
						$new_page_data['secondary_uri'] = $parent_page->secondary_uri;
						$new_page_data['tertiary_uri'] = $uri;
						$response['uri'] = '/'.$parent_page->primary_uri.'/'.$parent_page->secondary_uri.'/'.$uri;
						break;
				}

				$page = Page::create($new_page_data);

				# Create default page content component
				$response['success'] = 1;
				$response['page'] = $page->toArray();

			} catch (Exception $e) {

				$response['message'] = $e->getMessage();

			}
		}

		return Response::json($response);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the priority
	 * @return mixed
	 */
	public function updatePriority()
	{
		$response = array('success' => 0);

		if(Request::ajax())
		{

			try {

				$priorities = Input::all();

				foreach($priorities as $page_id => $priority)
				{
					DB::table('pages')
						->where('id', str_replace('page_', '', $page_id))
						->update(array('priority' => $priority));
				}

				Cache::forget('pages');

				$response['success'] = 1;

			} catch (Exception $e) {

				$response['message'] = $e->getMessage();

			}
		}

		return Response::json($response);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		try {

			$input = Input::all();

			# Need to check when changing URI

			$page = Page::find($id);
			$page->fill($input);
            $meta_tags = [];
            foreach($input['meta_tags'] as $meta_tag){
                $meta_tags[$meta_tag['name']] = $meta_tag['value'];
            }
            $page->meta_tags = json_encode($meta_tags);
			$page->save();

			$this->pageRepository->clearCacheById($page->id);

			$message = "Updated page!";

		} catch (Exception $e) {

			$message = $e->getMessage();

		}

		return Redirect::to(URL::previous())->with('message',$message);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		try {

			$response['success'] = 0;

			if(Input::get('confirm') !== "Delete")
			{
				throw new \Exception('Confirmation does not match');
			}

			$page = $this->pageRepository->delete($id);

			# Need to be able to remove all children pages

			$response['success'] = 1;
			$response['message'] = 'Removed Page '.$page->uri;

		}
		catch (Exception $e)
		{
			$response['message'] = $e->getMessage();
		}

		if(Request::ajax())
		{
			return Response::json($response);
		}
		else
		{
			if($response['success'])
			{
				return Redirect::route('admin.pages.index')->with('message', $response['message']);
			}
			else
			{
				return Redirect::to(URL::previous())->with('message',$response['message']);
			}
		}

	}

}