<?php namespace Liquidfish\Larafish\Page;

use Config, Cache, DB, Exception, View;
use Liquidfish\Larafish\Page\Component\Component as PageComponent;
use Liquidfish\Larafish\Page\Component\Data\Data as PageComponentData;

class Observer {

	/**
	 * Page Model Events
	 * @param Page $page
	 * @return void
	 * @throws Exception
	 */
	public function creating($page)
	{
		if(is_null($page->title)) throw new Exception("Must have a title");

		$page_check = DB::table('pages')->where('primary_uri',$page->primary_uri)->where('secondary_uri',$page->secondary_uri)->where('tertiary_uri',$page->tertiary_uri)->select('id')->first();

		if(!is_null($page_check))
		{
			throw new Exception("That page already exists");
		}

		if($page->nav_title == null) $page->nav_title = $page->title;

		if(empty($page->view))
		{
			$page->view = config('larafish.pages.default-view');
		}
	}

	/**
	 * Set default page view when creating a new Page
	 */
	public function saved($page)
	{
		Cache::forget('pages');
	}

	/**
	 * After Creating a new Page, we will create a content-area component with default data
	 */
	public function created($page)
	{
		# Create Page Component Data
		$data = array(
			'data' => View::make('larafish::components.default-content')->render()
		);

		$page_component_data = PageComponentData::create(array('data' => json_encode($data)));

		$component = array(
			'page_component_data_id' => $page_component_data->id,
			'page_id' => $page->id,
			'component_id' => 1
		);

		$page_component = PageComponent::create($component);

		Cache::forget('page:'.$page->uri.':published');
	}

	/**
	 * @param $page
	 */
	public function deleted($page)
	{
		# Delete child pages
		switch ($page->type) {
			case 'primary':
				DB::table('pages')->where('primary_uri','=',$page->primary_uri)->delete();
				break;
			case 'secondary':
				DB::table('pages')->where('primary_uri','=',$page->primary_uri)->where('secondary_uri','=',$page->secondary_uri)->delete();
				break;
		}

		Cache::forget('page:'.$page->uri.':published');
	}
} 