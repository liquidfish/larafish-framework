<?php namespace Liquidfish\Larafish\Page\Component;

use DB;
use Str;
use View;
use Illuminate\Support\Collection;

class ComponentBag {

	protected $components;

	function __construct(Collection $components)
	{
		$this->components = $components;
	}

	/**
	 * Add a named component
	 * @param string $view Name of the view
	 * @param array $data Key Value array of data
	 * @return void
	 */
	public function add($view, $data)
	{

		$name = str_slug( str_replace(array('::','.'), '-', $view) );

		while ( $this->components->offsetExists($name) ) # This component is already a view
		{
			$name = $name.'-'.$data['component']['id'];
		}

		try
		{
			$object = new \stdClass();
			$object->view = ( str_contains($view, '::') ? View::make($view, $data) : View::make('components.'.$view, $data) );
			$object->data = $data;
			$this->components->put($name, $object);
		}
		catch (\InvalidArgumentException $e)
		{

		}

	}

	/**
	 * Render specific View
	 * @param  string $title
	 * @return string        View rendered as a markup string
	 */
	public function render($title = null)
	{
		if( $this->components->offsetExists($title) )
		{
			return $this->components->get($title)->view;
		}
		else
		{
			return 'Component ['.$title.'] not set for this view';
		}
	}

	/**
	 * Render views
	 * @return string Views rendered to a string of markup
	 */
	public function all()
	{
		$response = '';
		foreach($this->components as $component)
		{
			$response .= $component->view->render();
		}
		return $response;
	}

	/**
	 * Return current components
	 * @return Collection
	 */
	public function current()
	{
		return $this->components;
	}

	/**
	 * Get all available components
	 * @return array
	 */
	public function available()
	{
		return DB::table('components')->lists('id','view');
	}

	/**
	 * Renders views except ones in the $arr array
	 * @param  [type] $arr  - Array of component id's to not render
	 * @return [type] string      [description]
	 */
	public function except($arr) {
		$response = '';
		foreach($this->components->except($arr) as $component)
		{
			$response .= $component->view->render();
		}
		return $response;
	}
    /**
     * Render all components of a specific type
     * @param  string $title
     * @return string        View rendered as a markup string
     */
    public function renderType($title = null)
    {
        $response = '';
        foreach($this->components as $component)
        {
            if($component->view->name() == $title) {
                $response .= $component->view->render();
            }
        }
        return $response;
    }

    /**
     * Renders views except ones whose type is in the $arr array
     * @param  [type] $arr  - Array of component titles to not render
     * @return [type] string      [description]
     */
    public function exceptType($arr) {
        $response = '';
        foreach($this->components as $component)
        {
            if(!in_array($component->view->name(), $arr)) {
                $response .= $component->view->render();
            }
        }
        return $response;
    }

}