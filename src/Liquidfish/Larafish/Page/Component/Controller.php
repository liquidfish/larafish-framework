<?php namespace Liquidfish\Larafish\Page\Component;

use Input;

class Controller extends \Illuminate\Routing\Controller {

	protected $componentRepository;

	function __construct(Repository $componentRepository)
	{
		$this->componentRepository = $componentRepository;
	}

	public function store()
	{
		$pageId = Input::get('page_id');
		$componentId = Input::get('component_id');
		return $this->componentRepository->add($pageId, $componentId);
	}
    public function sort()
    {
        $ids = explode(',',Input::get('ids'));
        $order = count($ids);
        foreach ($ids as $pageComponentId){
            $this->componentRepository->order($pageComponentId, $order);
            $order--;
        }
        return "success";
    }
	public function destroy()
	{
		$pageComponentId = Input::get('page_component_id');
		return $this->componentRepository->remove($pageComponentId);
	}

} 