<?php namespace Liquidfish\Larafish\Page\Component;

use DB;
use Liquidfish\Larafish\Events\ComponentAddedToPage;
use Liquidfish\Larafish\Events\ComponentRemovedFromPage;
use Liquidfish\Larafish\Page\Component\Component as PageComponent;
use Liquidfish\Larafish\Page\Component\Data\Data as PageComponentData;

class Repository {

	protected $pageRepository;

	/**
	 * Repository constructor.
	 * @param $pageRepository
	 */
	public function __construct(\Liquidfish\Larafish\Page\Repository $pageRepository)
	{
		$this->pageRepository = $pageRepository;
	}

	/**
	 * Create a Page Component
	 * @param $pageId
	 * @param $componentId
	 * @return \Liquidfish\Larafish\Page\Component\Component
	 */
	public function add($pageId, $componentId)
	{
		$data = PageComponentData::create(array('data' => json_encode(array( 'key' => 'value' )) ));

		$pageComponent = PageComponent::create(array( 'page_component_data_id' => $data->id, 'page_id' => $pageId, 'component_id' => $componentId ));

		$this->pageRepository->clearCacheById($pageId);

		event(new ComponentAddedToPage($pageComponent));

		return $pageComponent->load('component');
	}

	/**
	 * @param $pageComponentId
	 * @return \Liquidfish\Larafish\Page\Component\Component
	 */
	public function remove($pageComponentId)
	{
		$pageComponent = PageComponent::find($pageComponentId);
		$pageComponent->delete();

		event(new ComponentRemovedFromPage($pageComponent));

		$this->pageRepository->clearCacheById($pageComponent->page_id);

		# Delete page component data if it is only used once
		$count = DB::table('page_component_data')->where('id', $pageComponent->page_component_data_id)->count();

		if($count === 1)
		{
			DB::table('page_component_data')->where('id', $pageComponent->page_component_data_id)->delete();
		}

		return $pageComponent;
	}
    /**
     * @param $pageComponentId
     * @param $sortOrder
     * @return \Liquidfish\Larafish\Page\Component\Component
     */
    public function order($pageComponentId, $sortOrder)
    {
        $pageComponent = PageComponent::find($pageComponentId);
        $pageComponent->sort_order = $sortOrder;
        $pageComponent->save();

        $this->pageRepository->clearCacheById($pageComponent->page_id);

        return $pageComponent;
    }
} 