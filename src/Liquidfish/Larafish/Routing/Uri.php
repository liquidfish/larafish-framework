<?php namespace Liquidfish\Larafish\Routing;

use Request;
use Route;

class Uri {
	
	protected $ran = false;
	protected $primary_uri;
	protected $secondary_uri;
	protected $tertiary_uri;

	/**
	 * Determine the URI for the current requested route
	 * @return array Returns array with primary_uri, secondary_uri and tertiary_uri keys
	 */
	public function determine()
	{
		if(method_exists('Route', 'getCurrentRoute')) // Laravel 4.0 Routing Support
		{
			$route = Route::getCurrentRoute();
			$this->primary_uri = $route->getParameter('primary_uri');
			$this->secondary_uri = $route->getParameter('secondary_uri');
			$this->tertiary_uri = $route->getParameter('tertiary_uri');
		}
		else
		{
			$route = Route::current();
			$this->primary_uri = $route->parameter('primary_uri');
			$this->secondary_uri = $route->parameter('secondary_uri');
			$this->tertiary_uri = $route->parameter('tertiary_uri');
		}

		$this->ran = true;

		if($this->primary_uri == null) # We could be overriding via a custom controller
		{
			$segments = Request::segments();
			if(count($segments) === 0)
			{
				$this->primary_uri = null;
				$this->secondary_uri = null;
				$this->tertiary_uri = null;
			}
			else
			{
				isset($segments[0]) ? $this->primary_uri = $segments[0] : $this->primary_uri = null;
				isset($segments[1]) ? $this->secondary_uri = $segments[1] : $this->secondary_uri = null;
				isset($segments[2]) ? $this->tertiary_uri = $segments[2] : $this->tertiary_uri = null;
			}
		}

		return array('primary_uri' => $this->primary_uri, 'secondary_uri' => $this->secondary_uri, 'tertiary_uri' => $this->tertiary_uri);
	}

	public function getPrimary()
	{
		$this->check();
		return $this->primary_uri;
	}

	public function getSecondary()
	{
		$this->check();
		return $this->secondary_uri;
	}

	public function getTertiary()
	{
		$this->check();
		return $this->tertiary_uri;
	}

	private function check()
	{
		if(!$this->ran) $this->determine();
	}

}