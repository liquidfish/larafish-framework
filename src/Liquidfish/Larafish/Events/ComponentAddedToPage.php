<?php

namespace Liquidfish\Larafish\Events;

use Illuminate\Queue\SerializesModels;
use Liquidfish\Larafish\Page\Component\Component as PageComponent;

class ComponentAddedToPage extends \App\Events\Event
{
	use SerializesModels;

	/**
	 * @var PageComponent
	 */
	public $component;

	/**
	 * ComponentAddedToPage constructor.
	 * @param PageComponent $component
	 */
	public function __construct(PageComponent $component)
	{
		$this->component = $component;
	}
}
