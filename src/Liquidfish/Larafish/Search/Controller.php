<?php namespace Liquidfish\Larafish\Search;

use Input;
use View;
use Liquidfish\Larafish\Page\BaseController;

class Controller extends BaseController {

	protected $searchRepository;

	function __construct(Repository $searchRepository)
	{
		$this->searchRepository = $searchRepository;
	}

	/**
	 * Handle a search request
	 */
	public function search()
	{
		$query = Input::get('query',null);

		$results = $this->searchRepository->forQuery($query);
		$query = e($query);
		$this->layout->title = '"'.$query.'" search results';
		$this->layout->yield = View::make('larafish::search')->with('results', $results)->with('query', $query);

	}

}