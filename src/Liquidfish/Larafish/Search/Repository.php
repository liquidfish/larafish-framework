<?php namespace Liquidfish\Larafish\Search;

use Liquidfish\Larafish\Page\Repository as PageRepository;

class Repository {

	protected $pageRepository;

	function __construct(PageRepository $pageRepository)
	{
		$this->pageRepository = $pageRepository;
	}

	public function forQuery($query)
	{
		$results = array();
		$pageResults = $this->pageRepository->search($query);

		foreach($pageResults as $result)
		{
			$results[] = array(
				'title' => $result->title,
				'content' => $result->content,
				'uri' => $result->uri
			);
		}

		return $results;

	}
} 