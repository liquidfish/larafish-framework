<?php namespace Liquidfish\Larafish\Controllers;

use Liquidfish\Larafish\Page\BaseController;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class FileController extends BaseController {

	public $layout = 'larafish::admin.files.layout';
	public $upload_path;

	public function __construct(){
		$this->upload_path = public_path().'/uploads/';
	}

	public function createDirectory()
	{
		$message = 'asfd';

		try {

			$directory_name = str_slug(\Input::get('directory_name'));

			$directories = explode('.', \Input::get('in'));
			$path_to_create = $this->upload_path.implode('/', $directories).'/'.$directory_name;

			$fs = new Filesystem();
			$new_directory = $fs->makeDirectory($path_to_create,0777);

			$message = 'Directory '.$directory_name.' created';

		} catch (\IO\Exception $e) {
			$message = $e->getMessage();
		}

		return \Redirect::to(\URL::previous())->with('message',$message);

	}

	public function upload()
	{

		try {

			$message = '';

			$uploaded_files = count($_FILES);

			$files = \Input::file('files');

			if($files[0] != null)
			{

				foreach($files as $file)
				{

					$original_filename = $file->getClientOriginalName();

					$dir_to_create_in = \Input::get('in');

					$path = $this->upload_path;
					$path = $path.str_replace('.', '/', $dir_to_create_in);

					if(!is_dir($path))
					{
						throw new \Exception($path.' does not exist');
					}

					$original_extension = pathinfo($original_filename, PATHINFO_EXTENSION);
					$original_name = str_replace('.'.$original_extension, '', $original_filename);
					$original_name = str_replace('.', '-', $original_name);

					$new_filename = str_slug(str_replace('.'.$original_extension, '', $original_name));
					$new_filename = $new_filename.'.'.$original_extension;

					$file->move($path, $new_filename);

				}

					$message = 'File(s) uploaded';

			} else
			{
				throw new \Exception('No File');
			}

		} catch (\Exception $e) {

			$message = $e->getMessage();

		}

		return \Redirect::to(\URL::previous())->with('message',$message);

	}

	public function browser(){

		# Look for CKEditorFunNum
		if(\Input::has('CKEditorFuncNum'))
		{
			\Session::set('CKEditorFuncNum', \Input::get("CKEditorFuncNum"));
		}
		if(\Input::has('clearckeditor')) \Session::delete('CKEditorFuncNum');

		$path = \Input::get('path');

		$path_crumbs = array();
		if($path != null)
		{
			$path_crumbs = explode('.', $path);
		}

		$breadcrumbs = array();
		if(count($path_crumbs) > 0)
		{
			$breadcrumbs[] = '<a href="/admin/files/browser">Root</a>';

			foreach($path_crumbs as $crumb)
			{
				$temp_crumbs = $path_crumbs;

				$index = array_search($crumb, $temp_crumbs);

				if ($index !== false) {
					array_splice($temp_crumbs, ($index + 1));
				}

				$breadcrumbs[] = '<a href="/admin/files/browser?path='.implode('.', $temp_crumbs).'">'.$crumb.'</a>';
			}
		} else
		{
			$breadcrumbs[] = '<a href="/admin/files/browser">Root</a>';
		}

		$current_path = $this->upload_path;

		if(count($path_crumbs) > 0)
		{
			$current_path = $this->upload_path.implode('/', $path_crumbs);
		}

		$directory_finder = new Finder();
		$all_directories = $directory_finder->directories()->in($this->upload_path);

		$file_finder = new Finder();
		$files = $file_finder->in($current_path)->depth(0);

		\View::share('create_uri','?in='.implode('.', $path_crumbs));
		\View::share('current_path_uri', $path);
		
		$this->layout->path_crumbs = $path_crumbs;
		$this->layout->breadcrumbs = $breadcrumbs;

		$list_view = \View::make('larafish::admin.files.list');

		$list_view->file_count = iterator_count($files);
		$list_view->files = $files;
		$list_view->root_directory = $this->upload_path;
		$list_view->available_directories = $all_directories;

		$this->layout->yield = $list_view;

	}

	public function change()
	{

		$path = str_replace('.', '/', \Input::get('in'));

		$current_directory_path = $this->upload_path.$path;

		if(!is_writable($current_directory_path))
		{
			throw new \Exception($current_directory_path.' is not writable');
		}

		if(!file_exists($current_directory_path))
		{
			throw new \Exception($current_directory_path.' does not exist');
		}

		switch (\Input::get('operation')) {
			case 'move':
				
				try {

					$message = 'No files to move.';

					$files_to_move = \Input::get('files');
					$directories_to_move = \Input::get('directories');

					$new_directory_input = \Input::get('move_to');

					if($new_directory_input == '/') $new_directory_input = '';

					$new_directory_path = $this->upload_path.$new_directory_input;

					if(!file_exists($new_directory_path))
					{
						throw new \Exception($new_directory_path.' does not exist');
					}

					if(count($files_to_move))
					{

						foreach($files_to_move as $file)
						{
							$ffile = \File::move($current_directory_path.'/'.$file, $new_directory_path.'/'.$file);
						}

						$message = "Files moved.";

					}				

					if(count($directories_to_move))
					{

						foreach($directories_to_move as $directory_name)
						{

							$directory_path = $this->upload_path.$path.$directory_name;
							if(strpos($directory_path, $new_directory_path) === 0)
							{
								throw new \Exception('It is not possible to move a directory into one of its sub-directories');
							}

							var_dump($directory_path);
							var_dump($new_directory_path.'/'.$directory_name);

							rename($directory_path,$new_directory_path.'/'.$directory_name);

						}
						$message = 'Files and directories moved.';
					}


				} catch (\Exception $e) {
					$message = $e->getMessage();
				}

				return \Redirect::to(\URL::previous())->with('message',$message);

				break;
			
			case 'delete':
				
				try {

					$files_to_delete = \Input::get('files');
					$directories_to_delete = \Input::get('directories');

					$message = '';

					if(count($files_to_delete))
					{

						foreach($files_to_delete as $file)
						{
							if(is_file($current_directory_path.'/'.$file))
							{
								unlink($current_directory_path.'/'.$file);
							}
						}
						$message .= "Files deleted.";

					}

					if(count($directories_to_delete))
					{

						$path = str_replace('.', '/', \Input::get('in'));

						foreach($directories_to_delete as $directory_name)
						{
							$fs = new Filesystem();
							$fs->deleteDirectory($this->upload_path.$path.'/'.$directory_name);
						}

						$message .= " Directories deleted.";

					}

				} catch (\Exception $e) {
					$message = $e->getMessage();
				}

				return \Redirect::to(\URL::previous())->with('message',$message);

				break;

		}

	}

}
