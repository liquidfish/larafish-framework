<?php namespace Liquidfish\Larafish\Controllers;

use Larafish, Input, Redirect, View;
use Liquidfish\Larafish\Models\User;
use Liquidfish\Larafish\Models\Role;

class UserController extends \Liquidfish\Larafish\Page\BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$users = User::orderBy('id','desc')->with('roles');

		if(Input::has('query'))
		{
			$query = Input::get('query');
			$users->where('first_name','LIKE','%'.$query.'%')->orWhere('last_name','LIKE','%'.$query.'%');
		}

		$this->layout->title = "Users";
		$this->layout->yield = View::make('larafish::admin.users.list')->with('users', $users->paginate(20));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		// Need to get available roles and permissions

		$this->layout->title = 'Create User';

		if(Larafish::userHasRole('Architect'))
		{
			$roles = Role::all();
		}
		else
		{
			$roles = Role::where('id','!=',1)->get();
		}

		$this->layout->yield = View::make('larafish::admin.users.create')->with('roles',$roles);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		try {

			$input = Input::except('roles');
			$user = User::create($input);

			# Create Roles
			$roles = Input::get('roles');
			if(count($roles))
			{
				$user->roles()->sync($roles);
			}

			return Redirect::route('admin.users.edit',array($user->id))->withSuccessMessage('User Created!');

		} catch (Exception $e) {

			return Redirect::route('admin.users.create')->withSuccessMessage( $e->getMessage());
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$this->layout->title = 'Edit User';

		$user = User::find($id);
		$user->load('roles');

		$roles = Role::orderBy('name','desc');

		if(!Larafish::userHasRole('Architect'))
		{
			$roles->where('name','!=','Architect');
		}

		$this->layout->yield = View::make('larafish::admin.users.edit')->with('user',$user)->with('roles',$roles->get());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$user = User::find($id);
		$user->fill(Input::except('roles'));
		$user->save();

		# Sync Roles
		$roles = Input::get('roles');
		if(count($roles))
		{
			$user->roles()->sync($roles);
		}
		else
		{
			$user->roles()->detach();
		}

		return Redirect::route('admin.users.edit',array($user->id))->withSuccessMessage('User changes were saved');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(Input::get('confirm') === 'Delete')
		{
			try {
				$user = User::find($id);
				$user->delete();
				return Redirect::route('admin.users.index')->withSuccessMessage('User deleted');
			} catch (Exception $e) {
				return Redirect::route('admin.users.edit',array($user->id))->withFailureMessage( $e->getMessage());
			}
		}
		else
		{
			return Redirect::route('admin.users.edit',array($id))->withFailureMessage('Delete confirmation mismatch');
		}


	}

}