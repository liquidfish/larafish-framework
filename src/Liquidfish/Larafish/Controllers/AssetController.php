<?php namespace Liquidfish\Larafish\Controllers;

class AssetController {

    protected $js = array();
    protected $css = array();
    protected $string = '';
    protected $named = array();

    public function __construct()
    {
        $this->named = config('larafish.assets.named');
    }

    /**
     * View available named assets as defined by config
     * @return array Associative array of named assets
     */
    public function getNamed()
    {
        return $this->named;
    }

    public function all()
    {
        return array('css' => $this->css, 'js' => $this->js, 'string' => $this->string);
    }

    public function addJs($js)
    {
        array_push($this->js, $js);
    }

    /**
     * Add a named asset
     *
     * Named assets are defined in the larafish config, to modify, publish the config to your project:
     * php artisan config:publish liquidfish/larafish
     *
     * @param string $name The named asset
     */
    public function addNamed()
    {
        $assets = func_get_args();

        foreach($assets as $asset)
        {
            if(isset($this->named[$asset]))
            {
                $extension = $this->getExtension($this->named[$asset]);
                if($extension == 'js')
                {
                    $this->js[$asset] = $this->named[$asset];
                }
                if($extension == 'css')
                {
                    $this->css[$asset] = $this->named[$asset];
                }
            }
        }
    }

    /**
     * Determine the extension of the given asset
     * @param $path
     * @return mixed
     */
    protected function getExtension($path)
    {
        $qpos = strpos($path, "?");

        if ($qpos !== false)
        {
            $path = substr($path, 0, $qpos);
        }

        $extension = pathinfo($path, PATHINFO_EXTENSION);

        return $extension;
    }

    public function addLarafishJs($js)
    {
        if( !starts_with($js,'/') ) $js = '/'.$js;
        $this->addJs('/packages/liquidfish/larafish/assets'.$js);
    }

    public function addCss($css)
    {
        array_push($this->css, $css);
    }

    public function addLarafishCss($css)
    {
        if( !starts_with($css,'/') ) $css = '/'.$css;
        $this->addCss('/packages/liquidfish/larafish/assets'.$css);
    }

    public function appendString($appendage)
    {
        $this->string .= $appendage;
    }

    public function output()
    {
        $prefix = config('larafish.assets.prefix');
        $output = '';
        foreach($this->css as $css)
        {
            $output .= '<link href="'.( !starts_with($css,'//') ? $prefix : '' ).$css.'" rel="stylesheet">'."\n";
        }
        foreach($this->js as $js)
        {
            $output .= '<script src="'.( !starts_with($js,'//') ? $prefix : '' ).$js.'"></script>'."\n";
        }
        $output .= $this->string;

        return $output;
    }

}
