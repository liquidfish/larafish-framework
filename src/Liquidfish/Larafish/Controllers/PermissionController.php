<?php namespace Liquidfish\Larafish\Controllers;

use Liquidfish\Larafish\Models\Permission;

class PermissionController extends \Liquidfish\Larafish\Page\Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$permissions = Permission::orderBy('id','desc');

		if(\Input::has('query'))
		{
			$permissions->where('display_name','LIKE','%'.\Input::get('query').'%');
		}

		$this->layout->title = "Permissions";
		$this->layout->yield = \View::make('larafish::admin.permissions.list')->with('permissions', $permissions->paginate(20));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		// Need to get available roles and permissions
		
		$this->layout->title = 'Create Permission';
		$this->layout->yield = \View::make('larafish::admin.permissions.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		# Create Role
		$permission = Permission::create(\Input::all());

		return \Redirect::route('admin.permissions.edit',array($permission->id))->with('message','Permission Created!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$this->layout->title = 'Edit Permission';
		$permission = Permission::find($id);

		$this->layout->yield = \View::make('larafish::admin.permissions.edit')->with('permission', $permission);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$permission = Permission::find($id);
		$permission->fill(\Input::except('_method'));
		$permission->save();

		return \Redirect::route('admin.permissions.edit',array($permission->id))->with('message','Updated Permission');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$permission = Permission::find($id);
		$permission->delete();
		
		return \Redirect::route('larafish::admin.permissions.index')->with('message','Permission deleted');
	}

}