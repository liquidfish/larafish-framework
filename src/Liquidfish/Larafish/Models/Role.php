<?php namespace Liquidfish\Larafish\Models;

class Role extends \Eloquent {

	protected $guarded = array('id');

	/**
	 * Many-to-Many relations with Users
	 */
	public function users()
	{
		return $this->belongsToMany('Liquidfish\Larafish\Models\User', 'assigned_roles');
	}

	/**
	 * Many-to-Many relations with Permission
	 * named perms as permissions is already taken.
	 */
	public function permissions()
	{
		return $this->belongsToMany('Liquidfish\Larafish\Models\Permission');
	}

	/**
	 * Page Model Events
	 * @return void Manipulate Model at a certain event
	 */
	public static function boot()
	{
		parent::boot();

		/**
		 * Before delete all constrained foreign relations
		 */
		static::deleting(function($role)
		{
			try
			{
				\DB::table('assigned_roles')->where('role_id', $role->id)->delete();
				\DB::table('permission_role')->where('role_id', $role->id)->delete();
			} catch(Execption $e) {}
		});
	}

	/**
	 * Before save should serialize permissions to save
	 * as text into the database
	 *
	 * @param array $value
	 */
	// public function setPermissionsAttribute($value)
	// {
	// 	$this->attributes['permissions'] = json_encode($value);
	// }

	/**
	 * When loading the object it should un-serialize permissions to be
	 * usable again
	 *
	 * @param string $value
	 * permissoins json
	 */
	// public function getPermissionsAttribute($value)
	// {
	// 	return (array)json_decode($value);
	// }

	/**
	 * Save permissions inputted
	 * @param $inputPermissions
	 */
	public function savePermissions($inputPermissions)
	{
		if(! empty($inputPermissions)) {
			$this->permissions()->sync($inputPermissions);
		} else {
			$this->permissions()->detach();
		}
	}

	/**
	 * Attach permission to current role
	 * @param $permission
	 */
	public function attachPermission( $permission )
	{
		if( is_object($permission))
			$permission = $permission->getKey();

		if( is_array($permission))
			$permission = $permission['id'];

		$this->permissions()->attach( $permission );
	}

	/**
	 * Detach permission form current role
	 * @param $permission
	 */
	public function detachPermission( $permission )
	{
		if( is_object($permission))
			$permission = $permission->getKey();

		if( is_array($permission))
			$permission = $permission['id'];

		$this->permissions()->detach( $permission );
	}

}