<?php namespace Liquidfish\Larafish\Models;

class Permission extends \Eloquent {

	protected $guarded = array('id');

	public static function boot()
	{
		parent::boot();

		/**
		 * Create the name of the Permission from the display name
		 */
		static::saving(function($permission)
		{
			try
			{
				$permission->name = str_slug($permission->display_name,'_');
			} catch(\Execption $e) {}
		});

		/**
		 * Before delete all constrained foreign relations
		 */
		static::deleting(function($permission)
		{
			try
			{
				\DB::table('permission_role')->where('permission_id', $permission->id)->delete();
			} catch(\Execption $e) {}
		});

	}

	}