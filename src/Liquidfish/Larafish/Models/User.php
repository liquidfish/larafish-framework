<?php namespace Liquidfish\Larafish\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * Eloquent guarded parameters
	 * @var array
	 */
	protected $guarded = array('id');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}


	/**
	 * User Model Event hooks
	 * @return void
	 */
	public static function boot()
	{
		parent::boot();

		/**
		 * Check if we need to update the Hash of the User Password
		 */
		static::saving(function($user)
		{
			if(!empty($user->password))
			{
				if(\Hash::needsRehash($user->password))
				{
					$user->password = \Hash::make($user->password);
				}
			}
			else
			{
				unset($user->password);
			}
		});

	}

	/*
	----------------------------------------------------------
		Query Scopes
	----------------------------------------------------------
	*/

	public function scopeName($query)
	{
		return $query->where('votes', '>', 100);
	}

	public function password_resets()
	{
		return $this->belongsTo('Liquidfish\Larafish\Models\UserPasswordReset','user_id');
	}

	/*
	----------------------------------------------------------
		Roles and Permissions
	----------------------------------------------------------
	*/

	/**
	 * Many-to-Many relations with Role
	 */
	public function roles()
	{
		return $this->belongsToMany('Liquidfish\Larafish\Models\Role', 'assigned_roles');
	}

	/**
	 * Check if a user has a Role
	 * @param  array|string  $name The name(s) of the role
	 * @return boolean
	 */
	public function hasRole( $name )
	{
		$response = false;

		$names = array();

		if(!is_array($name))
		{
			$names[] = $name;
		}
		else
		{
			$names = $name;
		}

		if(count($names) === 0) $response = true;

		foreach ($this->roles as $role)
		{
			if(in_array($role->name, $names))
			{
				$response = true;
				break;
			}
		}

		return $response;
	}

	/**
	 * Check if user has a permission by its name
	 *
	 * @param string $permission Permission string.
	 *
	 * @access public
	 *
	 * @return boolean
	 */
	public function can( $permission )
	{
		foreach ($this->roles as $role) {
			// Validate against the Permission table
			foreach($role->permissions as $perm) {
				if($perm->name == $permission) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Checks role(s) and permission(s) and returns bool, array or both
	 * @param string|array $roles Array of roles or comma separated string
	 * @param string|array $permissions Array of permissions or comma separated string.
	 * @param array $options validate_all (true|false) or return_type (boolean|array|both) Default: false | boolean
	 * @return array|bool
	 * @throws InvalidArgumentException
	 */
	public function ability( $roles, $permissions, $options=array() ) {
		// Convert string to array if that's what is passed in.
		if(!is_array($roles)){
			$roles = explode(',', $roles);
		}
		if(!is_array($permissions)){
			$permissions = explode(',', $permissions);
		}

		// Set up default values and validate options.
		if(!isset($options['validate_all'])) {
			$options['validate_all'] = false;
		} else {
			if($options['validate_all'] != true && $options['validate_all'] != false) {
				throw new \InvalidArgumentException();
			}
		}
		if(!isset($options['return_type'])) {
			$options['return_type'] = 'boolean';
		} else {
			if($options['return_type'] != 'boolean' &&
				$options['return_type'] != 'array' &&
				$options['return_type'] != 'both') {
				throw new \InvalidArgumentException();
			}
		}

		// Loop through roles and permissions and check each.
		$checkedRoles = array();
		$checkedPermissions = array();
		foreach($roles as $role) {
			$checkedRoles[$role] = $this->hasRole($role);
		}
		foreach($permissions as $permission) {
			$checkedPermissions[$permission] = $this->can($permission);
		}

		// If validate all and there is a false in either
		// Check that if validate all, then there should not be any false.
		// Check that if not validate all, there must be at least one true.
		if(($options['validate_all'] && !(in_array(false,$checkedRoles) || in_array(false,$checkedPermissions))) ||
			(!$options['validate_all'] && (in_array(true,$checkedRoles) || in_array(true,$checkedPermissions)))) {
			$validateAll = true;
		} else {
			$validateAll = false;
		}

		// Return based on option
		if($options['return_type'] == 'boolean') {
			return $validateAll;
		} elseif($options['return_type'] == 'array') {
			return array('roles' => $checkedRoles, 'permissions' => $checkedPermissions);
		} else {
			return array($validateAll, array('roles' => $checkedRoles, 'permissions' => $checkedPermissions));
		}

	}

	/**
	 * Alias to eloquent many-to-many relation's
	 * attach() method
	 *
	 * @param mixed $role
	 *
	 * @access public
	 *
	 * @return void
	 */
	public function attachRole( $role )
	{
		if( is_object($role))
			$role = $role->getKey();

		if( is_array($role))
			$role = $role['id'];

		$this->roles()->attach( $role );
	}

	/**
	 * Alias to eloquent many-to-many relation's
	 * detach() method
	 *
	 * @param mixed $role
	 *
	 * @access public
	 *
	 * @return void
	 */
	public function detachRole( $role )
	{
		if( is_object($role))
			$role = $role->getKey();

		if( is_array($role))
			$role = $role['id'];

		$this->roles()->detach( $role );
	}

}
