<?php namespace Liquidfish\Larafish;

use Auth;
use View;

class Larafish {

	private $current_user = null;
	private $loggedIn = false;

	public function __construct()
	{
		$this->current_user = null;

		if (Auth::check()) {
			$this->current_user = Auth::user();
			$this->current_user->load('roles','roles.permissions');
			$this->loggedIn = true;
		}

		View::share('larafish', $this);

	}

	public function user()
	{
		return $this->current_user;
	}

	public function userCan($permission)
	{
		if(!$this->loggedIn) return false;
		return $this->current_user->can($permission);
	}

	public function userHasRole($name)
	{
		if(!$this->loggedIn) return false;
		return $this->current_user->hasRole($name);
	}

	public function userIsLoggedIn()
	{
		return $this->loggedIn;
	}

}