<?php namespace Liquidfish\Larafish\Operation;

use \Redirect;
use \Response;

class Operation {

	private $success = false;
	private $message;
	private $messageDetails = [];
	private $additionalData = [];

	/**
	 * Set Success
	 * @param boolean $bool
	 */
	public function succeeded()
	{
		$this->success = true;
	}

	/**
	 * Check if the Operation was successful
	 * @return bool
	 */
	public function wasSuccessful()
	{
		if($this->success)
		{
			return true;
		}
		return false;
	}

	/**
	 * Set the message for the Operation
	 * @param string $message
	 */
	public function setMessage($message)
	{
		$this->message = $message;
	}

	/**
	 * Get the message
	 * @return string
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * Set the message details
	 * @param array $details
	 */
	public function setMessageDetails(array $details)
	{
		$this->messageDetails = $details;
	}

	/**
	 * Get the message details
	 * @return array
	 */
	public function getMessageDetails()
	{
		return $this->messageDetails;
	}

	/**
	 * @param $key
	 * @param $value
	 */
	public function set($key,$value)
	{
		if(!empty($key))
		{
			$this->additionalData[$key] = $value;
		}
	}

	/**
	 * @param $key
	 * @param $value
	 */
	public function get($key)
	{
		if(isset($this->additionalData[$key]))
		{
			return $this->additionalData[$key];
		}
		return null;
	}

	/**
	 * Get the data
	 * @return array
	 */
	public function getData()
	{
		return array_merge([
			'success' => $this->success,
			'message' => $this->message,
			'messageDetails' => $this->messageDetails
		], $this->additionalData);
	}

	/**
	 * Return the data as a json string
	 * @return mixed
	 */
	public function asJson()
	{
		return json_encode($this->getData());
	}

} 