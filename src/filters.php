<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Role Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('admin', function()
{

	if(!Larafish::userHasRole('Admin') and !Larafish::userHasRole('Architect'))
	{
		return Redirect::guest('login');
	}
});

Route::filter('roles.architect', function()
{
	if(!Larafish::userHasRole('Admin'))
	{
		return Redirect::guest('login');
	}
});

Route::filter('roles.admin', function()
{
	if(!Larafish::userHasRole('Admin'))
	{
		return Redirect::guest('login');
	}
});
