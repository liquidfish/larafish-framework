<?php

return array(

	'pages' => array(
		'controller' => 'App\Http\Controllers\PageController',
		'default-view' => 'larafish::pages.default'
	),

	'layout' => array(
		'assets' => array(
			'use-defaults' => true
		)
	),

	'assets' => array(
		'prefix' => null,
		'named' => array(
			'jquery' => '//code.jquery.com/jquery-1.11.3.min.js',
			'jqueryui' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js',
			'ckeditor' => '/vendor/larafish/ckeditor/ckeditor.js',
			'admin' => '/vendor/larafish/js/admin.js',
			'admin-css' => '/vendor/larafish/css/admin.css',
            'print-css' => '/vendor/larafish/css/print.css',
			'page-manager' => '/vendor/larafish/js/PageManager.js',
			'semantic-js' => '//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.4/semantic.min.js',
			'semantic-css' => '//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.4/semantic.min.css',
			'sortable-js' => '//cdnjs.cloudflare.com/ajax/libs/Sortable/1.6.0/Sortable.min.js',
			'vuejs' => '//cdnjs.cloudflare.com/ajax/libs/vue/0.12.16/vue.js',
			'vuejs-resource' => '//cdnjs.cloudflare.com/ajax/libs/vue-resource/0.1.16/vue-resource.min.js',
			'vuejs-validation' => '//cdn.jsdelivr.net/vue.validator/1.4.4/vue-validator.min.js',
		)
	),

);