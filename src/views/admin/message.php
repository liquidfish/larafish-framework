<div class="ui <? if(isset($success)): ?> <?= ($success ? 'success' : 'error' ); endif ?> message">
	<i class="close icon"></i>
	<div class="header">
		<?= $message ?>
	</div>
	<? if(isset($message_details)): ?>
	<ul class="list">
		<? foreach($message_details as $detail): ?>
			<li><?= $detail ?></li>
		<? endforeach ?>
	</ul>
	<? endif ?>
</div>