<div class="larafish">
	<h1>Edit Permission</h1>

	<?
	echo Form::model($permission, array('method' => 'put', 'route' => array('admin.permissions.update', $permission->id), 'id' => 'form'));

	echo Form::label('display_name','Display Name');
	echo Form::text('display_name',$permission->display_name,array('required' => 'required'));

	echo Form::button('Save Changes',array('type' => 'submit'));

	echo Form::close();

	# Delete
	echo Form::model($permission, array('route' => array('admin.permissions.destroy', $permission->id),'method' => 'delete'));
	echo Form::button('Delete Permission',array('type' => 'submit'));
	echo Form::close();
	?>
</div>