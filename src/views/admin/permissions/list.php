<div class="larafish">
	<div class="ui secondary segment">

		<div class="ui grid">
			<div class="left floated three wide column">
				<a class="ui small green button" href="<?= URL::route('admin.permissions.create') ?>">
					<i class="plus icon"></i> Create Permission
				</a>
			</div>
			<div class="right floated four wide column">
				<form action="" class="ui fluid icon input" method="get">
					<input type="text" name="query" placeholder="Search..." value="<?= Input::get('query') ?>">
					<i class="search link icon"></i>
				</form>
			</div>
		</div>


		<table class="ui small table segment">
			<thead>
				<tr>
					<th></th>
					<th>Display Name</th>
					<th>Name</th>
				</tr>
			</thead>
			<tbody>
				<? foreach($permissions as $permission): ?>
				<tr>
					<td><a href="<?= URL::route('admin.permissions.edit', $permission->id) ?>" class="ui mini button">Edit</a></td>
					<td><?= $permission->display_name ?></td>
					<td><?= $permission->name ?></td>
				</tr>
				<? endforeach ?>
			</tbody>
		</table>

		<?= view('larafish::pagination', [ 'paginator' => $permissions ]) ?>
	</div>
</div>