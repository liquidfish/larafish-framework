<div class="larafish">
	<div class="ui secondary segment">

		<h1>Create Role</h1>

		<?= Form::open(array('route' => array('admin.roles.store'), 'id' => 'form')) ?>

		<div class="field">
			<?= Form::label('name','Name') ?>
			<?= Form::text('name',null,array('required' => 'required')) ?>
		</div>
		<table class="ui table segment">
			<thead>
				<tr>
					<td>Permissions</td>
				</tr>
			</thead>
			<tbody>
			<? foreach($permissions as $permission): ?>
				<tr>
					<td>
						<?= Form::checkbox('perms[]',$permission->id,null,array('id' => 'permission_'.$permission->id)) ?>
						<?= Form::label('permission_'.$permission->id,$permission->display_name,array('id' => 'permission_'.$permission->id)) ?>
					</td>
				</tr>
			<? endforeach ?>
			</tbody>
		</table>

		<?= Form::button('Create Role',array('type' => 'submit', 'class' => 'ui small green submit button')) ?>

		<?= Form::close() ?>

	</div>
</div>