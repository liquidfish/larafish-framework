<div class="ui small menu">
	<div class="menu">
		<div class="ui simple dropdown item">
			<i class="users icon"></i> Users <i class="dropdown icon"></i>
			<div class="menu">
				<a class="item" href="/admin/users"><i class="sidebar icon"></i> List</a>
				<a class="item" href="/admin/roles"><i class="key icon"></i> Roles</a>
				<a class="item" href="/admin/permissions"><i class="unlock alternate icon"></i> Permissions</a>
			</div>
		</div>
		<div class="ui simple dropdown item">
			<i class="sitemap icon"></i> Pages <i class="dropdown icon"></i>
			<div class="menu">
				<a class="item" href="/admin/pages"><i class="sidebar icon"></i> Manage</a>
				<a class="item" href="/admin/components"><i class="puzzle piece icon"></i> Components</a>
			</div>
		</div>
		<div class="right menu">
			<div class="item">
			Hey <?= $larafish->user()->first_name ?>
			</div>
			<a class="item" href="<?= URL::route('logout') ?>">
				Logout <i class="sign out icon"></i>
			</a>
		</div>

	</div>
</div>

<div class="ui center aligned segment" id="alert" style="position:fixed; top: 0; z-index: 300; display: none; width: 100%; box-shadow">
	Page has been edited! <button id="save" class="ui green button">Save</button> or
	<a id="discard" class="ui red button" href="">Discard</a>	the changes?
</div>