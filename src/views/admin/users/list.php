<div class="larafish">
	<div class="ui secondary segment">

		<div class="ui grid">
			<div class="left floated three wide column">
				<a class="ui small green button" href="/admin/users/create">
					<i class="plus icon"></i> Create User
				</a>
			</div>
			<div class="right floated four wide column">
				<form action="" class="ui fluid icon input" method="get">
					<input type="text" name="query" placeholder="Search..." value="<?= Input::get('query') ?>">
					<i class="search link icon"></i>
				</form>
			</div>
		</div>

		<? if($users->count()): ?>

			<table class="ui small table segment">
				<thead>
				<tr>
					<th></th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Username</th>
					<th>Role</th>
				</tr>
				</thead>
				<tbody>
				<? foreach($users as $user): ?>
					<tr class="">
						<td><a href="/admin/users/<?= $user->id ?>/edit" class="ui mini button">Edit</a></td>
						<td><?= $user->first_name ?></td>
						<td><?= $user->last_name ?></td>
						<td><?= $user->username ?></td>
						<td>
							<? foreach($user->roles as $role): ?>
								<?= $role->name ?>
							<? endforeach ?>
						</td>
					</tr>
				<? endforeach ?>
				</tbody>
			</table>
			<?= view('larafish::pagination', [ 'paginator' => $users ]) ?>

		<? else: ?>

			<h3>No users found.</h3>

		<? endif; ?>
	</div>
</div>