<div class="larafish">
	<div class="ui secondary segment">

		<h2 class="ui header">
			Edit User
		</h2>

		<div class="ui grid">

			<div class="eleven wide column">

				<?= Form::model($user, array('route' => array('admin.users.update', $user->id), 'method' => 'put', 'id' => 'form', 'class' => 'ui small form segment')) ?>

				<div class="field">
					<?= Form::label('first_name','First Name') ?>
					<?= Form::text('first_name',null,array('required' => 'required')) ?>
				</div>
				<div class="field">
					<?= Form::label('last_name','Last Name') ?>
					<?= Form::text('last_name',null,array('required' => 'required')) ?>
				</div>
				<div class="field">
					<?= Form::label('username','Username') ?>
					<?= Form::text('username',null,array('required' => 'required')) ?>
				</div>
				<div class="field">
					<?= Form::label('password','Password') ?>
					<input type="text" name="password">
				</div>

				<table class="ui table segment">
					<thead>
					<tr>
						<td>Roles</td>
					</tr>
					</thead>
					<tbody>
					<? foreach($roles as $role): ?>
						<tr>
							<td>
								<input type="checkbox" name="roles[]" value="<?= $role->id ?>" id="role_<?= $role->id ?>" <?= ( $user->roles->contains($role->id) ? 'checked="checked"' : '' ) ?>>
								<label for="role_<?= $role->id ?>"><?= $role->name ?></label>
							</td>
						</tr>
					<? endforeach ?>
					</tbody>
				</table>

				<?= Form::button('Save Changes',array('type' => 'submit', 'class' => 'ui small green submit button')) ?>

				<?= Form::close() ?>

			</div>
			<div class="five wide column">

				<?= Form::model($user, array('route' => array('admin.users.destroy', $user->id),'method' => 'delete', 'class' => 'ui form segment')) ?>

				<h4 class="ui header">Danger zone
					<div class="ui sub header">Type "Delete" to confirm</div>
				</h4>
				<div class="field">
					<input type="text" name="confirm"/>
				</div>
				<button class="ui mini red button">Delete</button>

				<?= Form::close() ?>

			</div>

		</div>

	</div>
</div>