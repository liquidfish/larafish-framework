<div class="larafish">
	<div class="ui secondary segment">

		<h1>Create User</h1>

		<?= Form::open(array('route' => array('admin.users.store'), 'id' => 'form', 'class' => 'ui small form segment')) ?>

		<div class="field">
			<?= Form::label('first_name','First Name') ?>
			<?= Form::text('first_name',null,array('required' => 'required')) ?>
		</div>
		<div class="field">
			<?= Form::label('last_name','Last Name') ?>
			<?= Form::text('last_name',null,array('required' => 'required')) ?>
		</div>
		<div class="field">
			<?= Form::label('username','Username') ?>
			<?= Form::text('username',null,array('required' => 'required', 'placeholder' => 'email@address.com')) ?>
		</div>
		<div class="field">
			<?= Form::label('password','Password') ?>
			<?= Form::text('password',null,array('required' => 'required')) ?>
		</div>
		<table class="ui table segment">
			<thead>
				<tr>
					<td>Roles</td>
				</tr>
			</thead>
			<tbody>
				<? foreach($roles as $role): ?>
				<tr>
					<td>
						<?= Form::checkbox('roles[]',$role->id, null, array('id' => 'role_'.$role->id)) ?>
						<label for="role_<?= $role->id ?>"><?= $role->name ?></label>
					</td>
				</tr>
				<? endforeach ?>
			</tbody>
		</table>

		<?= Form::button('Create User',array('type' => 'submit', 'class' => 'ui small green submit button')) ?>

		<?= Form::close() ?>

	</div>
</div>