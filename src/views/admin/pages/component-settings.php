
	<div class="ui segment">

		<h3>Current Components</h3>

		<div class="ui warning message" id="pleaseRefresh" style="display:none">
			Please refresh page to see Components changes
		</div>

		<table class="ui table segment" data-current-components>
			<thead>
			<tr>
				<th>Name</th>
				<th></th>
			</tr>
			</thead>
			<tbody>
			<? foreach($components as $title => $data): ?>
                <tr data-component-id="<?= $data->data['component']['id'] ?>">
                    <td><i class="sort icon component-sort-handle"></i> <?= $title ?></td>
                    <td><a class="ui tiny red button right floated" data-page-id="<?= $page_id ?>" data-component-id-to-remove="<?= $data->data['component']['id'] ?>"><i class="remove icon"></i>Remove</a></td>
                </tr>
			<? endforeach ?>
			</tbody>
		</table>
		<ul>

		</ul>

		<h3>Available Components</h3>
		<table class="ui mini table segment">
			<thead>
				<tr>
					<th>Name</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<? foreach($available as $view => $id): ?>
				<tr>
					<td><?= $view ?></td>
					<td><a class="ui tiny green button right floated" data-component-id-to-add="<?= $id ?>" data-page-id="<?= $page_id ?>"><i class="add icon"></i>Add</a></td>
				</tr>
				<? endforeach ?>
			</tbody>
		</table>
		<ul>

		</ul>

	</div>
