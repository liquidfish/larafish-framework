<div id="page_settings">

	<div class="ui fluid accordion">
		<div class="title">
			<i class="dropdown icon"></i> Page Settings
		</div>
		<div class="content">

			<div class="ui grid">
				<div class="eleven wide column">
					<?= Form::model($page, array('route' => array('admin.pages.update', $page->id), 'method' => 'put', 'class' => 'ui form segment')) ?>

					<div class="field">
						<?= Form::label('Title') ?>
						<?= Form::text('title') ?>
					</div>

					<div class="field">
						<?= Form::label('Published') ?>
						<?= Form::hidden('published',0) ?>
						<?= Form::checkbox('published', '1') ?>
					</div>

					<div class="field">
						<?= Form::label('View') ?>
						<?= Form::select('view', $available_views) ?>
						<? if(isset($message)) echo '<p>'.$message.'</p>' ?>
					</div>

					<div class="field">
						<?= Form::label('Redirect URL') ?>
						<?= Form::text('redirect_url') ?>
					</div>

                    <div class="field">
                        <div class="styled accordion">
                            <div class="title"><i class="dropdown icon"></i> Meta-tags</div>
                            <div class="content ui segment">
                                <?php $meta_tags = isset($page->meta_tags) ? json_decode($page->meta_tags, true) : [];
                                if(count($meta_tags) == 0) $meta_tags[''] = '';
                                $i = 0;
                                foreach ($meta_tags as $name => $value) : ?>
                                    <div class="meta-fieldset ui grid" data-metarow="<?= $i ?>">
                                        <div class="four wide column">
                                            <div class="field namefield">
                                                <?= Form::label('Name') ?>
                                                <?= Form::text('meta_tags['.$i.'][name]', $name) ?>
                                            </div>
                                        </div>
                                        <div class="ten wide column">
                                            <div class="field valuefield">
                                                <?= Form::label('Value') ?>
                                                <?= Form::text('meta_tags['.$i.'][value]', $value) ?>
                                            </div>
                                        </div>
                                        <div class="two wide column">
                                            <div style="font-size:2em;position:absolute;bottom:1rem">
                                                <a class="remove-meta-row"><i class="icon red circle remove"></i></a>
                                                <a class="add-meta-row"><i class="icon green circle add"></i></a>
                                            </div>

                                        </div>
                                    </div>
                                    <?php $i++;
                                endforeach; ?>
                                <span id="metarow_data" data-metarowcount="<?= $i ?>"></span>
                            </div>
                        </div>
                    </div>

					<?= Form::button('Update Settings',array('type' => 'submit', 'class' => 'ui green mini submit button')) ?>

					<?= Form::close() ?>
				</div>
				<div class="five wide column">
					<?= Form::model($page, array('route' => array('admin.pages.destroy', $page->id), 'method' => 'delete', 'class' => 'ui form segment')) ?>
						<h3 class="ui header">
							DangerZone
						</h3>
						<p>Confirm deletion by typing "Delete" below</p>
						<div class="field">
							<input type="text" name="confirm"/>
						</div>
						<button class="ui mini primary submit button"><i class="trash icon"></i> Delete this Page</button>
					<?= Form::close() ?>
				</div>
			</div>

			<? if(isset($component_settings)) echo $component_settings ?>

		</div>

	</div>

</div>