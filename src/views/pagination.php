<? if($paginator->lastPage() > 1): ?>
	<div class="ui small borderless pagination menu">
		<a href="<?= $paginator->url(1) ?>" class="item<?= ($paginator->currentPage() == 1) ? ' disabled' : '' ?>">
			<i class="icon left arrow"></i> Previous
		</a>
		<? for ($i = 1; $i <= $paginator->lastPage(); $i++): ?>
			<a href="<?= $paginator->url($i) ?>" class="item<?= ($paginator->currentPage() == $i) ? ' active' : '' ?>">
				<?= $i ?>
			</a>
		<? endfor ?>
		<a href="<?= $paginator->Url($paginator->currentPage()+1) ?>" class="item<?= ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' ?>">
			Next <i class="icon right arrow"></i>
		</a>
	</div>
<? endif ?>