<div class="larafish">
	<h1>Forgot Password</h1>
	<?
	echo Form::open(array('url' => URL::route('password-reset-request'), 'method' => 'post'));

	echo Form::label('username');
	echo Form::text('username', null, array('placeholder' => 'email@example.com'));

	echo Form::button('Email reset link', array('type' => 'submit'));

	echo Form::close();
	?>
</div>