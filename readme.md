# Installation

## Getting started from scratch (without using larafish-starter)

* Add ```liquidfish/larafish``` to your ```composer.json``` file. Since it is a private repo, you'll need to add the repository info too. [Example](https://bitbucket.org/liquidfish/larafish-starter/raw/master/composer.json)
* Make sure ```Liquidfish\Larafish\LarafishServiceProvider``` is in this providers of your ```app/config/app.php```
* Make sure you have a ```app/controller/PageController.php``` if not, copy [this one](https://bitbucket.org/liquidfish/larafish-starter/raw/master/app/controllers/PageController.php)
* Put ```Liquidfish\Larafish\Routing\DefaultRoutes::all();``` at the bottom of your ```app/routes.php``` file
* Change the user model ```Liquidfish\Larafish\Models\User``` in ```app/auth.php```
* Create ```app/views/pages/``` and ```app/views/components```. You will use these.
* Make sure you have a ```layout.php``` in your ```app/views/``` directory. Here is a [good starting point](https://bitbucket.org/liquidfish/larafish-starter/raw/master/app/views/layout.php). Look at the layout section below to see what things are required in your layout.

## Setup
* Add your database and credentials in ```app/database.php```
* Run ```php artisan larafish:setup``` from the command line
* Now login at ```http://larafish.dev/admin``` with an ```dev@liquidfish.com``` and the usual liquidfish password.
* You should be good to go!

## Upgrading from Larafish < 1.3
Component sorting is a new feature of Larafish 1.3. Because of that, the following changes may have to be manually applied.
* You'll need to copy 2017_08_29_182150_add_sorting_to_page_components_table.php to your migrations folder and migrate.
* Sortable JS is now part of the admin code, so copy over config.php from Larafish or make sure the following line is in your config.php named assets section:
`'sortable-js' => '//cdnjs.cloudflare.com/ajax/libs/Sortable/1.6.0/Sortable.min.js',`
* You'll also need an updated admin.js to activate and save component sorting.

# Pages
### Everything revolves around Pages in Larafish.  

Pages have Page Components.  

They are displayed like this (from larafish::pages.default):  
```
<?= $components->all() ?>
```

A page is given a ```$components``` object. This object contains all of that Page's Components.

```<?= $components->all() ?>``` will render all of the pages components in sequence.

If you want to render only a specific Component, you can do so like this:  
```
<div class="left">
	<?= $component->render('component-name') ?>
</div>
<div class="right">
	<?= $component->render('component-name-2') ?>
</div>
```
To see which components are available to your Page, look at the Page Settings > Available Components. You can then access the Component by its name.

Components are essentially views inside the ```views/components``` directory.

### How do I create Components?

Like this:
```php
<?= $component->open() ?>  
	<?= $component->slot('title') ?>  
	<?= $component->slot('description') ?>  
<?= $component->close() ?>  
```

```$component->slot('title')``` and ```$component->slot('description')``` will give you two inline-editable areas on your page. You can add as many slots as you want to.  

You can put anything you want in-between or around the ```$component->``` methods.

```php
<div class="wrapper">  
	<?= $component->open() ?>  
		<h1>Overview</h1>
		<div class="ui grid">
			<div class="column">
				<h3>Rules</h3>
				<?= $component->slot('rules') ?>  
			</div>
			<div class="column">
				<h3>Regulations</h3>
				<?= $component->slot('regulations') ?>  
			</div>
		</div>
	<?= $component->close() ?>  
</div>
```
Page Components have related Page Component Data. Page Component Data is formatted in json. This allows us to define our data structure from within our Component. So, above we use ```$component->slot('description')```. This creates an inline editable CKEditor instance. Once the data is edited in-page and saved, it will be saved into a json ```slot : value``` format in the page_component_data table.

### Adding attributes such as class to the component wrapper

Simply pass the attributes you want as an array to the open() function.
```php
<?= $component->open(['class'=>'class-for-this-thing']) ?>  
```

### Component slot options

$component->slot can take an array as a second parameter to set options.
As of Larafish 1.2.0, two options exist:

 * 'no-paragraph' will tell CKEditor not to create paragraphs by default when set to true. Defaults to false.
 * 'default-data' takes a string which will be displayed in the slot if no data has been set on it. Defaults to 'empty'
 
$options defaults to an empty array [], and then array_add is used to set the default value of these options if they haven't already been set. Passing a non-array will result in an error. 

## Create a Component
* Create a php file inside the ```views/components``` directory.
* For now, just put this in there:
```
<?= $component->open() ?>  
	<?= $component->slot('title') ?>  
	<?= $component->slot('description') ?>  
<?= $component->close() ?>  
```
* Now, log in at ```/admin```
* Go to the Pages > Components page from the main admin navigation.
* Now click [ Sync Components ]
* You should now see your component in the table below.

This will look at all the components in the ```views/components``` directory and sync them with the components table in the database.

## Adding a Component to a Page
* Go to the Page you want to add the Component to
* Under available components, [ + Add ]

*Note: If you do not see your component listed, make sure you have Synced Components.*

## Removing a Component to a Page
* Go to the Page you want to add the Component to
* Under available components, [ x Remove ]

# Routing

All the larafish default routes are contained within ```Liquidfish\Larafish\Routing\DefaultRoutes::all();```

This should be at the bottom of your ```routes.php``` file.

All of your custom routes should go in before it. Laravel routing is first-in first-out.


# Overriding default Larafish views
So, say you have the admin navigation bar, and you want to change the way it looks or add some more menu options.

You can override any view used in larafish by creating the ```views/packages/liquidfish/larafish/``` directory in your project.

So, to override the admin navigation, you would create ```views/packages/liquidfish/larafish/admin/navigation.php```

# Assets
You can manage assets throughout the view cycle.

Inside of a view, you can call ```Assets::addCss('/css/test.css')```

This will add a link attribute to the header of the ```layout.php```.

If you look inside of the layout.php file, you will see ```Assets::output()```. This adds all the assets that were defined throughout the request.

For Javascript:
```Assets::addJs('/path/to/js.js')```

### Named Assets
Named assets allow flexible assets so you can change the version of jquery without changing it throughout the entire app.

Open the ```app/config/packages/liquidfish/larafish/config.php```

You can change, or define new named assets within the ```assets > named``` array.

Named assets also limit an asset to being included only once. So, if you were to call ```Assets::addNamed('jquery')``` twice, ```Assets::output``` would only output jquery once.

### Asset Strings
Sometimes you need to add a string of code to the <head> of the document.

```Assets::appendString('<script>window.page_id = 15;</script>')```

Then you will be able to access this data from other scripts, etc.

## Layout (app/views/layout.php)
This is the master layout file for the entire application. You can change this file to suit the scope of the project you are working on.

Things to keep in layout.php:

* ```<?= Assets::output() ?>```
* ```<? if(isset($metatags)) echo $metatags ?>```
* ```<? if(isset($body_classes)) echo $body_classes ?>```
* ```<? if(isset($admin_navigation)) echo $admin_navigation ?>```
* ```<? if(isset($message)) echo $message ?>```
* ```<? if(isset($page_settings)) echo $page_settings ?>```
* ```<div id="page-content">```  
```	<?= $yield ?>```  
```</div>```